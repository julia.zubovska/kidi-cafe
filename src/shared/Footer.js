import React from 'react'
import {
  Container, Nav,
} from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite'
import logoBrand from '../assets/logo/logoBrand.png'
import iconIns from '../assets/inst.svg'
import iconFB from '../assets/face.svg'
import iconTwi from '../assets/twi.svg'
import Background from '../assets/menu/foot.png'

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'center',
    color: 'black',
  },
  footImg: {
    backgroundImage: `url(${Background})`,
  },
  logo: {
    width: '50px',
  },
  icon: {
    display: 'flex',
    marginRight: '2px',
    width: '17px',
  },
  backImg: {
    width: '40%',
    height: '178%',
    marginTop: '-78px',
  },
  test: {
    position: 'relative',
  },
  foot: {
    position: 'absolute',
    zIndex: -1,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    height: '100%',

  },
})

function Footer() {
  return (
    <div className={css(styles.test)}>

      <div className={css(styles.foot)}>
        <img src={Background} alt="background" className={css(styles.backImg)} />
      </div>

      <Container>

        <Nav className="justify-content-center" activeKey="/home">
          <Nav.Item>
            <Nav.Link href="#">
              <div className={css(styles.footer)}>
                <img className={css(styles.icon)} src={iconIns} alt="inst" />
                Instagram
              </div>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href="#">
              <div className={css(styles.footer)}>
                <img className={css(styles.icon)} src={iconTwi} alt="FB" />
                Twitter
              </div>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href="#">
              <div className={css(styles.footer)}>
                <img className={css(styles.icon)} src={iconFB} alt="FB" />
                Facebook
              </div>
            </Nav.Link>
          </Nav.Item>
          <Nav.Item />
        </Nav>
        <div className="text-center mt-4 mb-4">
          <img src={logoBrand} alt="cat" className={css(styles.logo)} />
        </div>
      </Container>
    </div>
  )
}

export default Footer
