import React from 'react'
import { NavLink } from 'react-router-dom'
import {
  Container, Navbar, Nav, Button,
} from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite'

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  head: {
    minHeight: '6%',
  },
  logo: {
    width: '50px',
  },
  icon: {
    display: 'flex',
    marginRight: '2px',
    width: '17px',
  },

})

function Header() {
  const activeStyle = {

    color: 'palevioletred',
    textDecoration: 'none',
  }

  const linkHead = {
    color: 'rgb(106 106 106)',
    textDecoration: 'none',
  }

  return (
    <div>
      <Navbar
        collapseOnSelect
        expand="sm"
        bg="light"
        variant="light"
        className={css(styles.head)}
      >
        <Navbar.Brand href="/" style={{ marginLeft: '15px' }}>
          Cat Cafe
        </Navbar.Brand>
        <Container>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">

              <Nav.Link>
                <NavLink
                  to="/"
                  style={({ isActive }) => (isActive ? activeStyle : linkHead)}

                >
                  Home
                </NavLink>

              </Nav.Link>
              <Nav.Link>
                {' '}
                <NavLink to="/gallery" style={({ isActive }) => (isActive ? activeStyle : linkHead)}>Gallery</NavLink>
              </Nav.Link>
              <Nav.Link>
                {' '}
                <NavLink to="/menu" style={({ isActive }) => (isActive ? activeStyle : linkHead)}>Menu</NavLink>
              </Nav.Link>
              <Nav.Link>
                {' '}
                <NavLink to="/review" style={({ isActive }) => (isActive ? activeStyle : linkHead)}>Reviews</NavLink>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <Nav>
            <Button variant="btn btn-outline-dark" className="me-2">
              Log in
            </Button>
            <Button variant="btn btn-outline-dark">Sing out</Button>
          </Nav>
        </Container>
      </Navbar>
    </div>
  )
}

export default Header
