import React, { useState } from 'react'
import { StyleSheet, css } from 'aphrodite'
import { Button, Container } from 'react-bootstrap'
import star from './pic/star.png'
import fatStar from './pic/fatStar.png'

const styles = StyleSheet.create({
  divCont: {
    marginRight: '160px',
    marginLeft: '160px',
  },
  userDiv: {
    display: 'flex',
  },
  userDivText: {
    paddingLeft: '40px',
  },
  starDivWrapper: {
    position: 'relative',
  },
  starDivContainer: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'row',
  },
  starDiv: {
    width: '13px',
    display: 'flex',
    paddingTop: '5px',
  },
  starImg: {
    width: '100%',
    height: '100%',
  },
  btn: {
    color: 'rgb(249 122 191)',
  },

})

const SCORE = [0, 1] // From database

function StarContainer() {
  const [score, setScore] = useState(SCORE)

  return (
    <div className={css(styles.starDivWrapper)}>
      <div className={css(styles.starDivContainer)}>
        {score.map((_, index) => (
          <div className={css(styles.starDiv)} role="button" tabIndex={0} onClick={() => setScore(index)} onKeyUp={() => {}}>
            <img src={fatStar} alt="fatStar" className={css(styles.starImg)} />
          </div>
        ))}

      </div>
      <div className={css(styles.starDivContainer)}>
        {[0, 1, 2, 3, 4].map((_, index) => (
          <div className={css(styles.starDiv)} role="button" tabIndex={0} onClick={() => setScore(index)} onKeyUp={() => {}}>
            <img src={star} alt="star" className={css(styles.starImg)} />
          </div>
        ))}
      </div>

    </div>
  )
}

function UserReviews() {
  return (
    <div>
      <Container>
        <div className={css(styles.divCont)}>
          <div className={css(styles.userDiv)}>
            <div>
              <span>
                Anastasia
                {' '}
                <br />
                {' '}
                21.02.2022
              </span>

              <StarContainer />

            </div>

            <div className={css(styles.userDivText)}>
              <p>
                Отличное кокосовое масло. Использую для волос,
                бровей и ресниц. Буду брать ещё ни раз

              </p>

              <Button variant="link" className={css(styles.btn)}>Ответить</Button>
            </div>

          </div>
          <hr />

          <div className={css(styles.userDiv)}>
            <div>
              <span>
                Victoria
                {' '}
                <br />
                {' '}
                02.02.2022
              </span>
              <div className={css(styles.starDiv)}>
                <img src={fatStar} alt="fatStar" className={css(styles.starImg)} />
                <img src={star} alt="star" className={css(styles.starImg)} />

              </div>
            </div>

            <div className={css(styles.userDivText)}>
              <p>
                Получила сегодня свою посылочку,
                масло отличного качества!
                Перед покупкой читала отзывы о продукции так как именно
                это масла не покупала раньше.
                Кто-то писал что масло плохое что оно с запахом смальца,
                подсолнечного масла...
                Решила взять на пробу маленький объем 25мл. и теперь пожалела,
                надо было брать 100 мл.
                В следующий раз возьму 100 мл. Масло превосходное,
                лишних посторонних
                запахов в нем нет ,
                аромат натуральный Кокосовый
                Огромное спасибо Мейкап за быструю доставку!

              </p>

              <Button variant="link" className={css(styles.btn)}>Ответить</Button>
            </div>

          </div>
          <hr />

        </div>
      </Container>
    </div>

  )
}

export default UserReviews
