import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import { Button, Container } from 'react-bootstrap'
import UserResponse from './UserResponse'

const styles = StyleSheet.create({
  divCont: {
    marginRight: '160px',
    marginLeft: '160px',
  },
  userDiv: {
    display: 'flex',
  },
  userDivText: {
    paddingLeft: '40px',
  },
  btn: {
    color: 'rgb(249 122 191)',
  },

})

function UserQuestions() {
  return (
    <div>
      <Container>
        <div className={css(styles.divCont)}>
          <div className={css(styles.userDiv)}>
            <div>
              <span>
                Polina
                {' '}
                <br />
                {' '}
                30.02.2022
              </span>
            </div>

            <div className={css(styles.userDivText)}>
              <p>
                Подскажите, можно ли это масло наносить на губы?

              </p>

              <Button variant="link" className={css(styles.btn)}>Ответить</Button>
            </div>

          </div>
          <UserResponse />

          <hr />

          <div className={css(styles.userDiv)}>
            <div>
              <span>
                Ann
                {' '}
                <br />
                {' '}
                02.01.2021
              </span>
            </div>

            <div className={css(styles.userDivText)}>
              <p>
                Такое и должно быть?
                Как его выдавать?
                Только выкалупывать
              </p>

              <Button variant="link" className={css(styles.btn)}>Ответить</Button>
            </div>

          </div>
          <UserResponse />
          <hr />

        </div>
      </Container>
    </div>

  )
}

export default UserQuestions
