import React, { useState } from 'react'
import { Container, Button } from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite'
import UserReviews from './UserReviews'
import UserQuestions from './UserQuestions'
import Background from './pic/heart.png'
import BackPaws from './pic/paws.png'
import FormReview from './form/FormReview'
import ReForm from './form/ReForm'

const styles = StyleSheet.create({
  div: {
    minHeight: '1200px',
  },
  h1Header: {
    textAlign: 'center',
  },
  btn: {
    display: 'flex',
    justifyContent: 'center',
    margin: '25px 0 25px 0 ',
  },

  RewQueDiv: {
    marginRight: '160px',
    marginLeft: '160px',
    marginBottom: '19px',
    borderRadius: '6px',
    height: '50px',
    background: 'rgba(236 101 146)',
  },
  RewQueSpan: {
    marginLeft: '15px',
    marginTop: '6px',
    bottom: '0',
  },
  backWrapper: {
    position: 'relative',
  },
  backDiv: {
    position: 'absolute',
    marginLeft: '820px',
    width: '1035px',
  },
  heartImg: {
    width: '100%',
  },
  pawsDiv: {
    position: 'absolute',
    width: '470px',
    marginTop: '-7px',
  },
  pawsImg: {
    width: '100%',
  },
})
// const users = [
//   {
//     name: 'fbgnh',
//   },
//   {},
//   {},
// ]

function Reviews() {
  const [reviews, setReviews] = useState(true)
  const [formRe, setFormRe] = useState(false)
  const [show, setShow] = useState(false)

  return (

    <div className={css(styles.div)}>
      <div className={css(styles.backWrapper)}>
        <div className={css(styles.pawsDiv)}>
          <img src={BackPaws} alt="paws" className={css(styles.pawsImg)} />
        </div>
        <div className={css(styles.backDiv)}>
          <img src={Background} alt="heart" className={css(styles.heartImg)} />
          {' '}
        </div>

        <h1 className={css(styles.h1Header)}>Our reviews</h1>

      </div>

      <Container>
        <div className={css(styles.backWrapper)}>
          <hr />
          <div className={css(styles.btn)}>
            <Button
              variant="outline-dark"
              onClick={() => {
                setFormRe(true)
              }}
            >
              {' '}
              Оставить отзыв

            </Button>
            


          </div>
          {formRe ? <FormReview setShow={setShow} setFormRe={setFormRe} /> : null}
          {show ? <ReForm /> : null}

          <div>
            <div className={css(styles.RewQueDiv)}>
              <Button
                variant="outline-light"
                className={css(styles.RewQueSpan)}
                onClick={() => setReviews(true)}
              >
                Reviews

              </Button>
              <Button
                variant="outline-light"
                className={css(styles.RewQueSpan)}
                onClick={() => setReviews(false)}
              >
                Questions

              </Button>
            </div>

            {reviews ? <UserReviews /> : <UserQuestions />}
            <div />
          </div>

        </div>
        
      </Container>
    </div>

  )
}

export default Reviews
