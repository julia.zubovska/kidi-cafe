import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import { Button, Container } from 'react-bootstrap'

const styles = StyleSheet.create({
  divContainer: {
    marginRight: '-11px',
    marginLeft: '70px',
  },
  userDiv: {
    display: 'flex',
  },
  userDivText: {
    paddingLeft: '40px',
  },
  btn: {
    color: 'rgb(249 122 191)',
  },
})

function UserResponse() {
  return (
    <div>
      <Container>

        <div className={css(styles.divContainer)}>
          <hr />
          <div className={css(styles.userDiv)}>
            <div>
              <span>
                CatCafe
                {' '}
                <br />
                {' '}
                31.02.2022
              </span>
            </div>

            <div className={css(styles.userDivText)}>
              <p>
                Бо

              </p>

              <Button variant="link" className={css(styles.btn)}>Ответить</Button>
            </div>

          </div>

        </div>
      </Container>
    </div>

  )
}

export default UserResponse
