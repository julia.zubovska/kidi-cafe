import React from 'react'
import { Container } from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite'

const styles = StyleSheet.create({
  div: {
    background: 'rgba(187, 187, 187, 0.4)',
    borderRadius: '12px',
    padding: '15px',
    textAlign: 'center',
  },
})

function ReForm() {
  return (
    <div>
      <Container>
        <div className={css(styles.div)}>
          <h3>Ваш отзыв успешно добавлен!</h3>
        </div>
      </Container>

    </div>
  )
}

export default ReForm
