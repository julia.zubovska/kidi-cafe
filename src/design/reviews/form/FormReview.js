import React from 'react'
import {
  Form, Button, Row, Col, Container,
} from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite'

const styles = StyleSheet.create({
  div: {
    background: 'rgba(187, 187, 187, 0.5)',
    borderRadius: '12px',
    marginBottom: '15px',
  },
  focusForm: {
    width: '50px',
    ':focus': {
      borderColor: 'red',
      boxShadow: 'none',
    },
  },
  divForm: {
    padding: '20px',
  },
})

function FormReview(props) {
  return (
    <div className={css(styles.div)}>

      <Container>
        <div className={css(styles.divForm)}>
          <Form>
            <div contentEditable сlassName={css(styles.focusForm)} />
            <Row className="justify-content-md-center">
              <Col xs={3}>

                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Твое имя</Form.Label>
                  <Form.Control aria-label="First name" rows={1} placeholder="введи свое имя" />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" placeholder="введи свой email" />

                </Form.Group>

                <div>
                  {' '}
                  <Form.Text className="text-black">
                    Хочешь оставить о нас отзыв или задать вопрос?
                  </Form.Text>
                  <Form.Check type="checkbox" label="Вопрос" />
                  <Form.Check type="checkbox" label="Отзыв" />
                </div>

              </Col>

              <Col xs={6}>
                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Текс</Form.Label>
                  <Form.Control as="textarea" rows={7} placeholder="можешь оставить о нас отзыв или задать нам вопрос" />
                  <Form.Text className="text-black"> Stars</Form.Text>
                </Form.Group>
              </Col>
              <Row className="justify-content-md-end">
                <Col md={3}>
                  <Button
                    variant="dark"
                    
                    onClick={() => { 
                      props.setShow(true) 
                      props.setFormRe(false) 
                    }}

                  >
                    Добавить отзыв
                  </Button>

                </Col>

              </Row>
            </Row>
          </Form>
        </div>
      </Container>

    </div>

  )
}

export default FormReview
