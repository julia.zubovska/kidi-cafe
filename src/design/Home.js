import { React } from 'react'
import { StyleSheet, css } from 'aphrodite'
import {
  Container, Row, Col,
} from 'react-bootstrap'
import '../mainStyle.css'
import cat from '../assets/cat/catWimdow.jpg'
import catCat from '../assets/cat/catCat.jpg'
import catHome from '../assets/cat/CateHome.png'
import catL from '../assets/cat/catL.jpg'
import BackgroundFono from '../assets/menu/fono.png'
import Background from '../assets/menu/fon.png'

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  backfon: {
    background: 'no-repeat',
    backgroundImage: `url(${BackgroundFono})`,
  },
  backImgDiv: {
    position: 'absolute',
    right: 0,
    zIndex: -1,
    marginTop: '-690px',
    width: 'auto',
    height: '1000px',
  },

  backImg: {
    widht: '100%',
    height: '100%',
  },

  body: {
    minHeight: '79%',
  },
  img: {
    width: '600px',
    marginBottom: '25px',
  },
})

function Home() {
  return (
    <div className={css(styles.backfon)}>

      <Container className={css(styles.body)}>
        <Row>
          <Col className="text-center mt-4 mb-4">
            <h1>Cat cafe</h1>
            <p>кафе для всей семьи</p>
            <hr />
            <h2>Немного о кототерапии</h2>
          </Col>
        </Row>

        <Row>
          <Col>
            <hr />
            <p>
              Cat Cafe – необычное заведение в центре Одессы, в котором живет 20
              настоящих кошек разных пород. Все фото наших любимцев можно посмотреть в
              {' '}
              <a href="/gallery">галереи</a>
              .
              Это место создано для общения с
              котами и отдыха под их мурлыканье. Каждый желающий может пройти
              здесь кототерапию и ощутить на себе ее целебные свойства.
            </p>

            <p>
              Cat Cafe – это светлое, теплое и уютное заведение для кошек,
              взрослых и детей. Это место идеально для проведения детских дней
              рождений или других праздников. Четырехлапые пушистики украсят
              собой любой Ваш праздник.
            </p>
          </Col>
          <Col>
            <div>
              <img src={catL} alt="cat" className={css(styles.img)} />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <div>
              <img src={catCat} alt="cat" className={css(styles.img)} />
            </div>
          </Col>
          <Col>
            <hr />
            <p>
              В Cat Cafe вы можете пообщаться с 20 кошечками разных пород, а
              также попробовать наши фирменные блюда, десерты и ароматный кофе!
              Здесь можно не только поиграть с котиками, но и вкусно и сытно
              поесть. Меню кошачьего кафе достаточно большое, способное
              удовлетворить любой вкус.
              Наше
              {' '}
              <a href="/menu">меню</a>
              {' '}
              .
            </p>

            <p>
              Гости Cat Cafe могут полакомиться пиццей, картофелем фри или
              бургерами, а также галицким борщом, стейками, блинами, рыбными
              блюдами и разнообразными десертами. Для взрослых большой выбор
              наливок, крафтового пива и барных напитков, а для детей разные
              безалкогольные коктейли и соки.
            </p>
          </Col>
        </Row>

        <Row>
          <Col>
            <hr />
            <p>
              Есть два зала с котиками, где оплата идет за время пребывания.
              Отдельно — зал, где размещается кофейня. Делать в антикафе можно
              все, только при этом не вредить котикам. Там можно выпить кофе,
              поработать за компьютером, а если устаешь — поиграться с котиками.
              Можно просто за ними наблюдать или погладить.
            </p>

            <p>
              Кафе рассчитано и на детей, но до 12 лет они могут находится с
              котами лишь под присмотром родителей. По выходным проводятся
              различные мастер-классы для деток.
            </p>
          </Col>
          <Col>
            <div>
              <img src={cat} alt="cat" className={css(styles.img)} />
            </div>
          </Col>
        </Row>

        <Row>
          <div className={css(styles.backImgDiv)}>
            <img src={Background} alt="imgage" className={css(styles.backImg)} />
          </div>

          <h2 className="text-center mt-4 mb-4">Немного о наших правилах</h2>
          <Col>
            <div>
              <img src={catHome} alt="cat" className={css(styles.img)} />
            </div>
          </Col>
          <Col>
            <hr />
            <p>
              Перед тем как зайти в зоны, где обитают котики необходимо надеть
              бахилы, а в ближайшее время появятся домашние тапочки. Также
              продезинфицировать руки антисептиком. Все это — для того, чтобы не
              передавать инфекцию котикам, которые очень восприимчивы.
            </p>
          </Col>
        </Row>
        <hr />

      </Container>

    </div>
  )
}

export default Home
