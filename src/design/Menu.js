import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite'
import MenuM from '../assets/menu/MenU.png'
import FullMenu from '../assets/menu/Menu.png'
import CatHead from '../assets/menu/catHead.png'

const styles = StyleSheet.create({
  container: {
    height: 'min1000px',

  },
  imgDiv: {
    flex: 'auto',
    display: 'flex',
    justifyContent: 'center',
  },
  img: {
    width: '900px',

  },
  imgCat: {
    width: '100%',
    marginTop: '-341px',
  },
  catDiv: {
    position: 'relative',
  },
  h1Cat: {
    position: 'absolute',
    bottom: '0',
    right: '56%',
  },
  backImg: {
    width: '100%',
    height: '100%',
  },

})

function Menu() {
  return (
    <div className={css(styles.container)}>
      <div>
        <Row>
          <Col>
            <div className={css(styles.catDiv)}>

              <img src={CatHead} className={css(styles.backImg)} alt="cat" />

              <h1 className={css(styles.h1Cat)}>Our Menu</h1>

            </div>

          </Col>
        </Row>
        <Container>
          <hr />

          <Row>
            <Col>
              <div className={css(styles.imgDiv)}>
                <img src={MenuM} alt="menu" className={css(styles.img)} />
              </div>
            </Col>
            <hr />
            <Col>
              <div className={css(styles.imgDiv)}>
                <img src={FullMenu} alt="menu" className={css(styles.img)} />
              </div>
            </Col>

          </Row>
          <hr />

        </Container>
      </div>
    </div>
  )
}

export default Menu
