import React from 'react'
import { Carousel, Container, Row } from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite'
import CatPhoto from '../assets/cat/gallery/catGe.jpg'
import CatPh from '../assets/cat/gallery/catmor.jpg'
import CatPho from '../assets/cat/gallery/citty.jpg'
import CatPhot from '../assets/cat/gallery/Caf.jpg'
import CatP from '../assets/cat/gallery/cafeCat.jpg'
import CatPH from '../assets/cat/gallery/catOn.jpg'
import CatPHO from '../assets/cat/gallery/9-luchshih-kafe-s-koshkami-5.jpg'
import BackgroundFono from '../assets/gallery/galleryPic.png'

const styles = StyleSheet.create({
  container: {
    height: '1000px',
  },
  carouselDiv: {
    height: '430px',
  },
  h1Gallery: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '50px',
    color: 'palevioletred',
  },
  backfon: {
    background: 'no-repeat',
    backgroundImage: `url(${BackgroundFono})`,
    height: '400px',
  },
  backFon: {
    background: 'no-repeat',
    backgroundImage: `url(${BackgroundFono})`,
    position: 'absolute',
    zIndex: '-1',
    display: 'flex',
    height: '400px',
    width: '300px',
    marginLeft: '1103px',
    marginTop: '-219px',
  },
  h4Gallery: {
    paddingTop: '40px',
    paddingLeft: '200px',
    paddingRight: '200px',
    textAlign: 'center',
  },

})

function Gallery() {
  return (
    <div>
      <Container className={css(styles.container)}>
        <Row className="justify-content-md-center" lg="2">

          <Carousel fade variant="light">
            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 "
                src={CatPho}
                alt="First slide"
              />

            </Carousel.Item>
            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 "
                src={CatPhoto}
                alt="Second slide"
              />

            </Carousel.Item>
            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 "
                src={CatPh}
                alt="Third slide"
              />

            </Carousel.Item>

            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 "
                src={CatPhot}
                alt="Third slide"
              />

            </Carousel.Item>

            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 "
                src={CatP}
                alt="Third slide"
              />

            </Carousel.Item>

            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 "
                src={CatPH}
                alt="Third slide"
              />

            </Carousel.Item>

            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 "
                src={CatPHO}
                alt="Third slide"
              />

            </Carousel.Item>

          </Carousel>
        </Row>

        <div>

          <h1 className={css(styles.h1Gallery)}>Our Gallery</h1>
          <hr />

          <div className={css(styles.backfon)}>

            <div>
              <h4 className={css(styles.h4Gallery)}>
                {' '}
                Кошачье кафе - прекрасное место, где можно выпить вкусный кофе,

                попробовать домашнюю выпечку и сделать это в компании дружелюбных и мягких котиков.
                <br />
                Мы пригласим вас в волшебное место, где, пройдя через гардероб,

                вы попадете в «КОШАЧИЙ РАЙ»,

                где хозяевами являются коты.
                <br />
                Здесь можно почитать книгу, газету или просто пообщаться с котом.

              </h4>
              <div className={css(styles.backFon)} />

            </div>
          </div>

        </div>
        <hr />

      </Container>

    </div>

  )
}

export default Gallery
