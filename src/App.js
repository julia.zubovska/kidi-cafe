import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { StyleSheet, css } from 'aphrodite'
import Home from './design/Home'
import Gallery from './design/Gallery'
import Menu from './design/Menu'
import Reviews from './design/reviews/Reviews'
import Header from './shared/Header'
import Footer from './shared/Footer'

const styles = StyleSheet.create({
  root: {
    height: '100vh',
    margin: 0,
    padding: 0,
  },
  container: {
    height: '100%',
  },
  head: {
    minHeight: '6%',
  },
  foot: {
    minHeight: '15%',
    backgroundColor: 'pink',
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'center',
    color: 'black',
  },
  logo: {
    width: '50px',
  },
  icon: {
    display: 'flex',
    marginRight: '2px',
    width: '17px',
  },
})

function App() {
  return (
    <div className={css(styles.root)}>
      <Header />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/gallery" element={<Gallery />} />
        <Route path="/menu" element={<Menu />} />
        <Route path="/review" element={<Reviews />} />
      </Routes>

      <Footer />

    </div>

  )
}

export default App
